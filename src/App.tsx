import { useEffect, useState } from "react";

import Card from "./components/Card";
import Table from "./components/Table/Table";

import { ProductData } from "./types/dataTypes";

import "./App.scss";
import MarketDataStore from "./store/marketDataStore";

function App() {
  const [productsData, setProductsData] = useState<{
    [key: string]: ProductData;
  }>({});
  const marketDataStore = new MarketDataStore();


  useEffect(() => {
    async function fetchInstruments() {
      const res = await fetch("https://api.delta.exchange/v2/products?page_size=200");
      if (res.status === 200) {
        const data = await res.json();
        if (data && data?.result && data.result.length > 0) {
          const newData: { [key: string]: ProductData } = {};
          data.result.forEach(({ symbol, description, underlying_asset }: any) => {
            newData[symbol] = {
              symbol: symbol,
              description: description,
              underlyingAsset: underlying_asset.symbol,
            };
          });
          setProductsData(newData);
        } else {
          console.log("No data found");
        }
      } else {
        console.error(res.statusText);
      }
      return () => {
        console.log("cleanup");
      };
    }
    fetchInstruments();
  }, []);

  return (
    <div className="App">
      <Card>
        <Table rowData={productsData} marketDataStore={marketDataStore}/>
      </Card>
    </div>
  );
}

export default App;
