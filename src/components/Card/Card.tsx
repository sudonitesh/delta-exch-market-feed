import { FC } from "react";

import "./card.scss";

interface IProps {
  children: React.ReactNode;
}

const Card: FC<IProps> = ({ children }) => (
  <div className="card">{children}</div>
);

export default Card;
