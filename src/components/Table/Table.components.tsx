import { FC, memo } from "react";
import { ProductData } from "../../types/dataTypes";

// Table Cell Component
interface ICellProps {
  content: string | number;
  className?: string;
}

const TableCellComponent: FC<ICellProps> = ({ content, className = "td" }) => (
  <td className={className}>{content}</td>
);

// Table Header Component
type TableHeader = {
  name: string;
  key: keyof ProductData | "ltp";
};

const TableHeadComponent: FC = () => {
  const productTableHeader: TableHeader[] = [
    { name: "Symbol", key: "symbol" },
    { name: "Market Price", key: "ltp" },
    { name: "Underlying Asset", key: "underlyingAsset" },
    { name: "Description", key: "description" },
  ];
  return (
    <thead>
      <tr>
        {productTableHeader.map(({ name, key }) => (
          <th key={key} className={`${key}-header`}>
            {name}
          </th>
        ))}
      </tr>
    </thead>
  );
};

// Table Row Component

interface IRowProps {
  children: JSX.Element;
  instrumentData: ProductData;
}

const TableRowComponent: FC<IRowProps> = ({
  children,
  instrumentData,
}: IRowProps) => {
  return (
    <tr>
      <TableCell className="fixed-column" content={instrumentData["symbol"]} />
      {children}
      <TableCell content={instrumentData["underlyingAsset"]} />
      <TableCell
        className="description-column"
        content={instrumentData["description"]}
      />
    </tr>
  );
};

export const TableCell = memo(TableCellComponent);
export const TableHead = memo(TableHeadComponent);
export const TableRow = TableRowComponent;
