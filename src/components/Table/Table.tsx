import { FC, useEffect } from "react";
import useWebSocket from "react-use-websocket";

import { ProductData } from "../../types/dataTypes";

import "./table.scss";
import { TableCell, TableHead, TableRow } from "./Table.components";
import { observer } from "mobx-react-lite";
import MarketDataStore from "../../store/marketDataStore";

interface IProps {
  rowData: { [key: string]: ProductData };
  marketDataStore: MarketDataStore;
}

const Table: FC<IProps> = ({ rowData, marketDataStore }) => {
  const marketDataSocketUrl = "wss://production-esocket.delta.exchange";
  const { lastJsonMessage, sendJsonMessage } = useWebSocket(marketDataSocketUrl);

  useEffect(() => {
    // Send heartbeat message every 25 seconds to keep connection alive
    sendJsonMessage({
      type: "enable_heartbeat",
    });
    const sendHeartBeatInterval = setInterval(() => {
      console.log("sending beat");
      sendJsonMessage({
        type: "enable_heartbeat",
      });
    }, 25_000);
    return () => {
      clearInterval(sendHeartBeatInterval);
    };
  }, [sendJsonMessage]);

  useEffect(() => {
    if (lastJsonMessage !== null) {
      if (lastJsonMessage?.type === "v2/ticker") {
        marketDataStore.updateStocksData(
          lastJsonMessage.symbol,
          lastJsonMessage.turnover_symbol,
          lastJsonMessage.mark_price
        );
      }
    }
  }, [lastJsonMessage]);

  useEffect(() => {
    // Subscribe to instruments after fetching data
    sendJsonMessage({
      type: "subscribe",
      payload: {
        channels: [{ name: "v2/ticker", symbols: Object.keys(rowData) }],
      },
    });
  }, [rowData, sendJsonMessage]);

  // Table micro-structured to avoid re-rendering 
  return (
    <div className="tableWrapper">
      <table className="table">
        <TableHead />
        <tbody>
          {Object.keys(rowData).map((symbol) => (
            <TableRow key={symbol} instrumentData={rowData[symbol]}>
              <TableCell content={marketDataStore.instrumentsLTP[symbol] || "--"} />
            </TableRow>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default observer(Table);
