import { makeAutoObservable } from "mobx";
import { numberFormatter } from "../utils/textFormatters";

class MarketDataStore {
  instrumentsLTP: { [key: string]: string } = {};
  currencyFormatterUSD = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    maximumSignificantDigits: 2,
    minimumFractionDigits: 2,
  });
  currencyFormatterINR = new Intl.NumberFormat("en-IN", {
    style: "currency",
    currency: "INR",
    maximumSignificantDigits: 2,
    minimumFractionDigits: 2,
  });

  constructor() {
    makeAutoObservable(this);
  }

  marketPriceFormatter = (
    mark_price: string,
    turnover_symbol: string
  ): string => {
    const marketPrice = Number(mark_price);
    if (isNaN(marketPrice)) return "--";

    if (turnover_symbol === "USD") {
      return this.currencyFormatterUSD.format(marketPrice);
    } else if (turnover_symbol === "INR") {
      return this.currencyFormatterINR.format(marketPrice);
    }
    return `${numberFormatter(marketPrice)} ${turnover_symbol}`;
  };

  updateStocksData(symbol: string, turnover_symbol: string, mark_price: string) {
    this.instrumentsLTP[symbol] = this.marketPriceFormatter(
      mark_price,
      turnover_symbol
    );
  }
}

export default MarketDataStore;
