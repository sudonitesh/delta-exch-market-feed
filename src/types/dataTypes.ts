export type ProductData = {
  symbol: string;
  description: string;
  underlyingAsset: string;
};
