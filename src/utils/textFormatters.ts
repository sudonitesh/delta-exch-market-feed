export const numberFormatter = (value: Number): string => {
  const currency = Number(value);
  if (isNaN(currency)) return "--";
  return currency.toLocaleString("en-US", {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
  });
};
